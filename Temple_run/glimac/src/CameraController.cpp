#include "glimac/CameraController.hpp"

namespace gWorld {

    void CameraController::initialiseCamPos(){
        _camTEyes->initialise(1.f, 0.,0.); 
        _camF->initialise();
        _camT->initialise(-6.f,15.,0.);
    };

    void CameraController::onEvent(const glimac::SDLWindowManager& windowManager, const SDL_Event& e) {
        switch(e.type) {
            case SDL_KEYDOWN:
                switch(e.key.keysym.sym) {
                    case SDLK_i:    // réinitialisation de la position de la camera
                        if (!_camIsBlocked){ 
                            initialiseCamPos();
                        }
                        break;
                    case SDLK_c:    //changement de vue (retour à la position initiale de la cam)
                        if (!_camIsBlocked){
                            _camActive = _camActive +2;
                            if (_camActive>3)
                                _camActive = 1;
                            initialiseCamPos();
                        }
                        break;
                    case SDLK_p:    //récupération de la position de la caméra
                        if (_camActive == 1)
                            _camTEyes->getPosition();
                        else if (_camActive == 2)
                            _camF->getPosition();
                        else 
                            _camT->getPosition();
                        break;
                    case SDLK_l:    //immobilisation de la camera
                        _camIsBlocked = !_camIsBlocked; 
                        break;
                    
                }
                break;

            case SDL_MOUSEMOTION:
                if (windowManager.isMouseButtonPressed(SDL_BUTTON_LEFT) && !_camIsBlocked) {
                    if (e.motion.xrel != 0) {
                        if(_camActive == 3)
                            _camT->rotateLeft(-e.motion.xrel / 1.5f);
                        else if(_camActive == 2)
                            _camF->rotateLeft(-e.motion.xrel / 1.5f);
                        else 
                            _camTEyes->rotateLeft(e.motion.xrel / 1.5f);
                    }
                    if (e.motion.yrel != 0) {
                        if (_camActive == 3)
                            _camT->rotateUp(-e.motion.yrel / 1.5f);
                        else if(_camActive == 2)
                            _camF->rotateUp(-e.motion.yrel / 1.5f);
                        else 
                            _camTEyes->rotateUp(e.motion.yrel / 1.5f);
                    }
                } 

            default:
                break;
        }
    
        if (e.type == SDL_MOUSEBUTTONUP && !_camIsBlocked){
            if (e.button.button == SDL_BUTTON_WHEELUP){
                _camT->moveFront(-pasT/5);
                _camTEyes->moveFront(pasT/5);
            } else if (e.button.button == SDL_BUTTON_WHEELDOWN){
                _camT->moveFront(pasT/5);
                _camTEyes->moveFront(-pasT/5);
            }
        }
    }

    glm::mat4 CameraController::getViewMatrix() const {
        if (_camActive == 3)
            return _camT->getViewMatrix();
        else if (_camActive == 1)
            return _camTEyes->getViewMatrix();
        return _camF->getViewMatrix();
    }
    
};