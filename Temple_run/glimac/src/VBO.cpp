#include "glimac/VBO.hpp"

namespace openGL {

    VBO::VBO(gWorld::Shape &shape){
        glGenBuffers(1, &m_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

        glBufferData (GL_ARRAY_BUFFER, shape.getVertexCount() * sizeof(glimac::ShapeVertex), shape.getDataPointer(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    };

    VBO::~VBO(){
        glDeleteBuffers(1,&m_vbo);
    };


}