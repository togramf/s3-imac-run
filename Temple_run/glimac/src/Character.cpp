#include <cmath>
#include <iostream>
#include "glimac/common.hpp"
#include "glimac/Character.hpp"

namespace gWorld {

        Character::Character () : 
            m_Position(0,0,0), 
            m_FrontVector (cos(m_fAngleX) * sin(m_fAngleY), sin(m_fAngleX), cos(m_fAngleX) * cos(m_fAngleY)),
            m_LeftVector(sin(m_fAngleY + (M_PI / 2)), 0, cos(m_fAngleY + (M_PI / 2))),
            m_centered(true), m_running(false), m_jumping(false), 
            m_left(false), m_right(false), m_squatting(false)
        {
            m_body = new Sphere(1,15,15);
            m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
        };

        //gestions des observers
        void Character::add    (Camera& cam) {
            m_Observers.push_back(&cam);
        };
        void Character::remove (Camera& cam) {
            m_Observers.remove(&cam);
        };
        void Character::notify () {
            for (Camera* cam : m_Observers){
                cam->updateTargetPos(m_Position);
            }
        };
        
        //mouvements
        void Character::start(){
            m_running = !m_running;
        };

        void Character::moveFront(float t){
            m_Position += t * m_FrontVector;
            notify();
        };

        void Character::right (){
            if (m_running && m_centered && !m_squatting){
                m_right = !m_right;
            }
        };

        void Character::left (){
            if (m_running && m_centered && !m_squatting){
                m_left = !m_left;
            }
        };

        void Character::moveLeft(float t){
            if (m_right){
                if((m_Position + t * m_LeftVector).x < 3.0)
                    m_Position += t * m_LeftVector;
                else 
                    m_right = false;
            }else if (m_left){
                if((m_Position + t * m_LeftVector).x > -3.0)
                    m_Position += t * m_LeftVector;
                else 
                    m_left = false;
            } else {
                if ((m_Position + t * m_LeftVector).x!=0)
                    m_Position += t * m_LeftVector;
            }
            centered();
            notify();
        };

        void Character::jump(){
            if (m_running && !m_squatting && m_centered){
                m_jumping = !m_jumping;
            }
        };

        void Character::squat(){
            if (m_running && !m_jumping && m_centered)
                m_squatting = !m_squatting;
        };
            
        void Character::moveUp(float t){
            if ((m_Position + t * m_UpVector).y>0.0 && (m_Position + t * m_UpVector).y<3.0){
                m_Position += t * m_UpVector;
                notify();
            } else 
                m_jumping = false;
            centered();
        };

        void Character::centered (){
            float epsilon = 0.2;
            if (m_Position.x < epsilon && m_Position.x > -epsilon && m_Position.y < epsilon)
                m_centered = true;
            else 
                m_centered = false;
        }

        //scores
        void Character::updateNbCoin(int valeur){
            m_NbCoins+=valeur;
        };

        //getter
        const glm::vec3 Character::getPosition(){
            return m_Position;
        };

        const int Character::getNbCoins(){
            return m_NbCoins;
        };

        const glimac::ShapeVertex* Character::getBodyPointer() const{
            return m_body->getDataPointer();
        };

        GLsizei Character::getVertexCount() const{
            return m_body->getVertexCount();
        };

        bool Character::getState(const std::string input) const {
            if (input.compare("running")==0)
                return m_running;
            else if (input.compare("jumping")==0)
                return m_jumping;
            else if (input.compare("squatting")==0)
                return m_squatting;
            else if (input.compare("right")==0)
                return m_right;
            else if (input.compare("left")==0)
                return m_left;
            
            return false;
        };

}
   
