#include <cmath>
#include "glimac/common.hpp"
#include "glimac/Coin.hpp"

namespace gWorld {

        Coin::Coin () : m_Position(0.0,0.0,0.0), m_catched(false) {
            m_body = new Sphere(1,5,5);
            m_value = rand() % 10 + 1;
        };

        Coin::Coin (glm::vec3 Position) : m_catched(false) {
            m_body = new Sphere(1,5,5);
            m_Position = Position;
            m_value = rand() % 10 + 1;
        };

        //fonction 
        void Coin::isCatched(){
            if (!m_catched)
                m_catched = !m_catched;
        };

        //getter
        const glm::vec3 Coin::getPosition() {
            return m_Position;
        };

        const int Coin::getValue() const {
            return m_value;
        };

        const glimac::ShapeVertex* Coin::getBodyPointer() const {
            return m_body->getDataPointer();
        };

        GLsizei Coin::getVertexCount() const {
            return m_body->getVertexCount();
        };

        bool Coin::getCatched() const {
            return m_catched;
        };
            
}
   
