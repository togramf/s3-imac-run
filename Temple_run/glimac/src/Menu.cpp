#include "glimac/Menu.hpp"
#include <iostream>
#include <charconv>




void SDL_GL_RenderText(const char *text, TTF_Font *font,SDL_Color color,SDL_Rect *location) {
	SDL_Surface *initial;
	SDL_Surface *intermediary;
	int w,h;
	GLuint texture;
	
	//Utilise SDL_TTF pour afficher le texte
	initial = TTF_RenderText_Blended(font, text, color);

	w= initial->w;
	h=initial->h;
	
	intermediary = SDL_CreateRGBSurface(0, w, h, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);

	SDL_BlitSurface(initial, 0, intermediary, 0);
	
	//Donne les nouvelles textures à GL 
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_BGRA, 
			GL_UNSIGNED_BYTE, intermediary->pixels );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	

	//Prépare à rendre les textures 
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	glColor3f(1.0f, 1.0f, 1.0f);
	
	//Dessine un quadrilatère à la position voulue 
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 1.0f); 
			glVertex2f(location->x    , location->y);
		glTexCoord2f(1.0f, 1.0f); 
			glVertex2f(location->x + w, location->y);
		glTexCoord2f(1.0f, 0.0f); 
			glVertex2f(location->x + w, location->y + h);
		glTexCoord2f(0.0f, 0.0f); 
			glVertex2f(location->x    , location->y + h);
	glEnd();
	glFinish();
	
	location->w = initial->w;
	location->h = initial->h;
	
	//Nettoie
	SDL_FreeSurface(initial);
	SDL_FreeSurface(intermediary);
	glDeleteTextures(1, &texture);
}

void glEnable2D() {

	int vPort[4];
  
	glGetIntegerv(GL_VIEWPORT, vPort);
  
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
  
	glOrtho(0, vPort[2], 0, vPort[3], -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
}

void glDisable2D() {

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();   
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();	
}

TTF_Font* initializeTTF(std::string fontpath, int size){
    TTF_Font* font;
	int err;
 
	//gestion d'erreurs
    if(err=TTF_Init()){
		TTF_GetError();
	    atexit(TTF_Quit);
    }
	
	if(!(font = TTF_OpenFont(fontpath.c_str(), size))) {
		printf("Error loading font: %s", TTF_GetError());
	}

    return font;
}



void DrawMenu(TTF_Font *font2,TTF_Font *font1,int screenwidth, int screenheight, int MenuType, int Score){
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable2D();
	glDisable(GL_DEPTH_TEST);

	SDL_Color color;
	SDL_Rect position;

	//Entête du Menu
	position.x = screenwidth / 3.3;
	position.y = screenheight /1.5;

	color.r = 255;
	color.g = 0;
	color.b = 255;
	
	SDL_GL_RenderText("Kirby Run", font2, color, &position);

	
	position.y -= position.h;
	color.r = 255;
	color.g = 255;
	color.b = 255;

	//Menu de Lancement de jeu
	if(MenuType==0){
		position.x = screenwidth / 2.8;

		SDL_GL_RenderText("Jouer          ECHAP", font1, color, &position);
		position.y -= position.h;
		SDL_GL_RenderText("Score          S", font1, color, &position);
		position.y -= position.h;
		SDL_GL_RenderText("Regles         I", font1, color, &position);
		position.y -= position.h;
		

 	}
	
	//Menu pour afficher le score
	else if(MenuType==1){
		position.x = screenwidth / 2.8;
		
    	std::string temp_str=std::to_string(Score); 
    	char const* Score = temp_str.c_str();
		SDL_GL_RenderText("Score  ", font1, color, &position);
		position.x += 150;
		SDL_GL_RenderText(Score, font1, color, &position);
		

	}
	//Menu pour afficher les règles
	else if(MenuType==2){
		position.x = screenwidth / 4.5;
		SDL_GL_RenderText("Le but est de ramasser un maximum de pieces !!", font1, color, &position);
				position.y -= position.h*2;
		SDL_GL_RenderText("Commandes : ", font1, color, &position);
				position.y -= position.h*2;
		SDL_GL_RenderText("Lancer le jeu                        SPACE", font1, color, &position);
				position.y -= position.h;
		SDL_GL_RenderText("Sauter                                Z", font1, color, &position);
				position.y -= position.h;
		SDL_GL_RenderText("Se deplacer                          Q D", font1, color, &position);
				position.y -= position.h; 
		SDL_GL_RenderText("Changer point de vue                C", font1, color, &position);
				position.y -= position.h;
		SDL_GL_RenderText("Bloquer point de vue                L", font1, color, &position);
				position.y -= position.h;
		SDL_GL_RenderText("Reinitialiser point de vue           I", font1, color, &position);
				position.y -= position.h;



	}
	//Menu de fin de jeu
	else if(MenuType==3){
	color.r = 255;
	color.g = 0;
	color.b = 255;
	
	SDL_GL_RenderText("FINISH !", font2, color, &position);
	
	position.x = screenwidth / 2.8;
	position.y -= position.h;
	color.r = 255;
	color.g = 255;
	color.b = 255;
		
	std::string temp_str=std::to_string(Score); 
	char const* Score = temp_str.c_str();
	SDL_GL_RenderText("Score  ", font1, color, &position);
	position.x += 150;
	SDL_GL_RenderText(Score, font1, color, &position);

	}

	//Come out of HUD mode 
	glEnable(GL_DEPTH_TEST);
	glDisable2D();
}


void CloseFont(TTF_Font *font){
    TTF_CloseFont(font);
}
