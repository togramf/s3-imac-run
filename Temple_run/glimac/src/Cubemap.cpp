#include "glimac/Cubemap.hpp"

#include <GL/glew.h>
#include <vector>
#include <iostream>

#include "glimac/glm.hpp"

namespace gWorld {

    Cubemap::Cubemap(const std::string pathProject, const std::string map) {
        
        GLfloat vertices[] = {
            // Positions
            -1.0f, 1.0f, -1.0f,     -1.0f, -1.0f, -1.0f,     1.0f, -1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,     1.0f, 1.0f, -1.0f,     -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,     -1.0f, -1.0f, -1.0f,    -1.0f, 1.0f, -1.0f,
            
            -1.0f, 1.0f, -1.0f,     -1.0f, 1.0f, 1.0f,      -1.0f, -1.0f, 1.0f,
             1.0f, -1.0f, -1.0f,     1.0f, -1.0f, 1.0f,      1.0f, 1.0f, 1.0f,
             1.0f, 1.0f, 1.0f,       1.0f, 1.0f, -1.0f,      1.0f, -1.0f, -1.0f,

            -1.0f, -1.0f, 1.0f,     -1.0f, 1.0f, 1.0f,       1.0f, 1.0f, 1.0f,
             1.0f, 1.0f, 1.0f,       1.0f, -1.0f, 1.0f,     -1.0f, -1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,      1.0f, 1.0f, -1.0f,      1.0f, 1.0f, 1.0f,
            
             1.0f, 1.0f, 1.0f,      -1.0f, 1.0f, 1.0f,      -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,    -1.0f, -1.0f, 1.0f,      1.0f, -1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,    -1.0f, -1.0f, 1.0f,      1.0f, -1.0f, 1.0f
        };

        m_size_vertices = sizeof(vertices);
        
        for (uint i=0; i<m_size_vertices; ++i){
            m_vertices.push_back(vertices[i]);
        }
        
        m_faces.push_back(pathProject+"/assets/textures/cubemap/"+map+"/right.jpg");
        m_faces.push_back(pathProject+"/assets/textures/cubemap/"+map+"/left.jpg");
        m_faces.push_back(pathProject+"/assets/textures/cubemap/"+map+"/top.jpg");
        m_faces.push_back(pathProject+"/assets/textures/cubemap/"+map+"/bottom.jpg");
        m_faces.push_back(pathProject+"/assets/textures/cubemap/"+map+"/front.jpg");
        m_faces.push_back(pathProject+"/assets/textures/cubemap/"+map+"/back.jpg");
    }
}