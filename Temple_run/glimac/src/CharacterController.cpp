#include "glimac/CharacterController.hpp"

namespace gWorld {

    void CharacterController::onEvent (const glimac::SDLWindowManager& windowManager, const SDL_Event& e){
        switch(e.type) {
            case SDL_KEYDOWN:
                switch(e.key.keysym.sym) {
                    case SDLK_SPACE:    //start and stop
                        if (!_character->getState("running")){
                            _character->start();
                        }
                        break;
                    case SDLK_z:        //saute
                        if (_character->getPosition().y < 0.2){
                            _character->jump();
                        }
                        break;
                    case SDLK_s:    //se baisse - ne fonctionne pas encore
                        // _character->squat();
                        // std::cout << "shift" <<std::endl;   
                        break;
                    case SDLK_q:    //tourne ou se déplace à gauche
                        if (!_character->getState("left")){
                            _character->left();
                        }
                        break;
                    case SDLK_d:    //tourne ou se déplace à droite
                        if (!_character->getState("right")){
                            _character->right();
                        }
                        break;
                }
                break;  
            default:
                break;
        }
    };
    
    void CharacterController::update(std::vector<gWorld::Coin*> &coinsPosition, std::vector<glm::vec3> &wallsPositions, bool &endGame){

        for(uint32_t i=0; i<coinsPosition.size(); ++i){
            if (!coinsPosition[i]->getCatched()){
                glm::vec3 diff = _character->getPosition()-coinsPosition[i]->getPosition();
                if(abs(diff.x)<0.8 && abs(diff.y)<0.8 && abs(diff.z)<0.8 ) {
                    _character->updateNbCoin(coinsPosition[i]->getValue());
                    coinsPosition[i]->isCatched();
                    std::cout << "+ "<<coinsPosition[i]->getValue()<<std::endl;
                }
            }
        }

        for (uint32_t i=0; i<wallsPositions.size(); ++i){
            glm::vec3 diff = _character->getPosition()-wallsPositions[i];
            if (abs(diff.z)<0.5 && abs(diff.y)<1.0) {
                if (abs(diff.x)<2.5){
                    endGame = true;
                    std::cout << " Collision MORTELLE avec un mur ! "<<std::endl;
                }
            }
        }

        if (_character->getState("running")){
            _character->moveFront(_speed);
        }
        if (_character->getState("jumping")){
            _character->moveUp(-_speed);
        } else if (_character->getPosition().y>0.0){
            _character->moveUp(_speed);     //fait descendre le personnage
        }
        if (_character->getState("right")){
            _character->moveLeft(-_speed);
        } else if (_character->getPosition().x>0.0){
            _character->moveLeft(_speed);
        }
        if (_character->getState("left")){
            _character->moveLeft(_speed);
        } else if (_character->getPosition().x<0.0){
            _character->moveLeft(-_speed);
        }
    };
}