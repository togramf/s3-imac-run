#include "../include/glimac/glm.hpp"
#include "../include/glimac/FreeflyCamera.hpp"
#include <iostream>

namespace gWorld {
        
    void FreeflyCamera::computeDirectionVectors(){
        m_FrontVector = glm::vec3(cos(m_fAngleX) * sin(m_fAngleY), 
                            sin(m_fAngleX), 
                            cos(m_fAngleX) * cos(m_fAngleY));

        m_LeftVector = glm::vec3(sin(m_fAngleY + (M_PI / 2)), 
                            0, 
                            cos(m_fAngleY + (M_PI / 2)));

        m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
    }

    //constructeur et destructeur
    FreeflyCamera::FreeflyCamera() : m_Position(0., 1, 6.f), Camera(0.0, M_PI){
        computeDirectionVectors();
    };
    FreeflyCamera::FreeflyCamera(const glm::vec3 position) : m_Position(position), Camera(0.0, M_PI){
        computeDirectionVectors();
    };

    //méthodes de déplacement 
    void FreeflyCamera::moveFront(const float t){
        m_Position += t * m_FrontVector;
        computeDirectionVectors();
    };

    void FreeflyCamera::moveLeft(const float t){
        m_Position += t * m_LeftVector;
        computeDirectionVectors();
    };

    void FreeflyCamera::rotateLeft(const float degrees){
        m_fAngleY += glm::radians(degrees);
        computeDirectionVectors();
    };

    void FreeflyCamera::rotateUp(const float degrees){
        m_fAngleX += glm::radians(degrees);
        computeDirectionVectors();
    };

    void FreeflyCamera::getPosition(){
        std::cout << m_Position << std::endl;
    };

    void FreeflyCamera::initialise(){
        m_Position = glm::vec3(0.,1,6.f);
        m_fAngleX = 0.;
        m_fAngleY = M_PI;
        computeDirectionVectors();
    };

    //getter
    glm::mat4 FreeflyCamera::getViewMatrix() const {
        glm::vec3 w_Position = m_Position + m_targetPosition; //position dans le monde
        return glm::lookAt(w_Position, w_Position + m_FrontVector, m_UpVector);
    };
        
}