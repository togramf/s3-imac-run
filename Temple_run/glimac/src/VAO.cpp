#include "glimac/VAO.hpp"

namespace openGL {

    VAO::VAO(VBO &vbo, GLuint attr_pos, GLuint attr_normal,
                GLuint attr_coord) {
                    
        glGenVertexArrays(1, &m_vao);
        glBindVertexArray(m_vao);

        const GLuint VERTEX_ATTR_POSITION = attr_pos;
        const GLuint VERTEX_ATTR_NORMALE  = attr_normal;
        const GLuint VERTEX_ATTR_COORDS   = attr_coord;

        glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
        glEnableVertexAttribArray(VERTEX_ATTR_COORDS);
        glEnableVertexAttribArray(VERTEX_ATTR_NORMALE);

        glBindBuffer(GL_ARRAY_BUFFER, *vbo.getVBO());
        glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const void*)(offsetof(glimac::ShapeVertex,position)));
        glVertexAttribPointer(VERTEX_ATTR_NORMALE , 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const void*)(offsetof(glimac::ShapeVertex,normal)));
        glVertexAttribPointer(VERTEX_ATTR_COORDS  , 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const void*)(offsetof(glimac::ShapeVertex,texCoords)));
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

    };

    VAO::~VAO(){
        glDeleteVertexArrays(1, &m_vao);
    };


}