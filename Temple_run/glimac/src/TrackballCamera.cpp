#include "../include/glimac/glm.hpp"
#include <iostream>
#include "../include/glimac/TrackballCamera.hpp"

namespace gWorld {

    //constructeur et destructeur
    TrackballCamera::TrackballCamera() : m_fDistance(1.f), Camera(0.0,0.0) {};
    TrackballCamera::TrackballCamera(const float d,const float x,const float y) : m_fDistance(d), Camera(x,y){};

    //méthodes de déplacement 

    void TrackballCamera::moveFront(const float t){
        if (m_fDistance+t >=1.0)
            m_fDistance += t;
    };

    void TrackballCamera::moveLeft(const float t){};

    void TrackballCamera::rotateLeft(const float degrees){
        if (m_fAngleY+degrees > -70.0 && m_fAngleY+degrees < 70.0)
            m_fAngleY += degrees;
    };

    void TrackballCamera::rotateUp(const float degrees){
        if (m_fAngleX+degrees > -50.0 && m_fAngleX+degrees < 50.0)
            m_fAngleX += degrees;
    };

    void TrackballCamera::getPosition(){
        glm::vec3 position(m_fDistance*cos(m_fAngleX)*sin(m_fAngleY),
                        m_fDistance*sin(m_fAngleY),
                        m_fDistance*cos(m_fAngleX)*cos(m_fAngleY));
        std::cout << position << std::endl;
    };

    void TrackballCamera::initialise(){
        m_fDistance = 1.f;
        m_fAngleX   = 0.f;
        m_fAngleY   = 0.f;
    };

    void TrackballCamera::initialise(const float d,const float x,const float y){
        m_fDistance = d;
        m_fAngleX   = x;
        m_fAngleY   = y;
    };

    /**
     * @brief Get the View Matrix object
     * 
     * @return glm::mat4 
     */
    glm::mat4 TrackballCamera::getViewMatrix() const{
        glm::mat4 VMatrix = glm::mat4(1.0);
        VMatrix = glm::translate(VMatrix, glm::vec3(0.,0.,m_fDistance));
        VMatrix = glm::rotate   (VMatrix, glm::radians(m_fAngleX), glm::vec3(1, 0, 0)) ;
        VMatrix = glm::rotate   (VMatrix, glm::radians(m_fAngleY), glm::vec3(0, 1, 0));
        VMatrix = glm::translate(VMatrix, -m_targetPosition);
        return VMatrix;
    };

}