#include "glimac/Skybox.hpp"

GLuint CreateSkyboxVAO(){
	GLuint skyboxVAO; 
    glGenVertexArrays(1, &skyboxVAO);
    glBindVertexArray(skyboxVAO);
	return skyboxVAO;
}

GLuint CreateSkyboxVBO(gWorld::Cubemap skybox){
	GLuint skyboxVBO; 
	glGenBuffers(1, &skyboxVBO);
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
    glBufferData(GL_ARRAY_BUFFER, skybox.getVerticesLength(), skybox.getVerticesPointer(), GL_STATIC_DRAW);
	return skyboxVBO;
}

GLuint CreateModelSkybox(std::string pathProject_current,gWorld::Cubemap* skybox){
    unsigned int CubeMaptexture;
    glGenTextures(1, &CubeMaptexture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, CubeMaptexture);

    int width, height, nrChannels;
    for (unsigned int i = 0; i < skybox->getFacesLength(); i++)
    {
        unsigned char *data = stbi_load(skybox->getFace(i).c_str(), &width, &height, &nrChannels, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
        }
        else
        {
            std::cout << "Cubemap texture failed to load at path: " << skybox->getFace(i) << std::endl;
            stbi_image_free(data);
        }
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return CubeMaptexture;
}

void DrawSkybox(std::string applicationPath, GLuint skyboxVAO, 
        GLuint skyboxVBO, GLuint cubemapTexture, 
        glm::mat4 MVMatrix, glm::mat4 ProjMatrix){
    
    //chargement des shaders
    SkyBoxProgram skyBoxProgram(applicationPath);
    skyBoxProgram.m_Program.use();

    glUniformMatrix4fv(skyBoxProgram.uMVMatrix,  1, GL_FALSE, glm::value_ptr(MVMatrix));
    glUniformMatrix4fv(skyBoxProgram.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(ProjMatrix));

    glBindVertexArray(skyboxVAO);
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Remet la fonction de profondeur par défaut
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);

    // Change la fonction de profondeur pour que le test de profondeur 
    // reussisse quand les valeurs sont égales à celle du contenu du buffer
    glDepthFunc(GL_LEQUAL);

}

void freeSkybox(GLuint* skyboxVAO, GLuint* skyboxVBO){
    glDeleteBuffers(1, skyboxVBO);
    glDeleteVertexArrays(1, skyboxVAO);
}

