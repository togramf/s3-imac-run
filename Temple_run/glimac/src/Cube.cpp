#include <cmath>
#include <vector>
#include <iostream>
#include "glimac/common.hpp"
#include "glimac/Cube.hpp"

namespace gWorld {

void Cube::build(const GLfloat size) {
    GLfloat vertices[] = {
            -1.0f, 1.0f, -1.0f,     -1.0f, -1.0f, -1.0f,     1.0f, -1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,     1.0f, 1.0f, -1.0f,     -1.0f, 1.0f, -1.0f,

            -1.0f, -1.0f, 1.0f,     -1.0f, -1.0f, -1.0f,    -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,     -1.0f, 1.0f, 1.0f,      -1.0f, -1.0f, 1.0f,

             1.0f, -1.0f, -1.0f,     1.0f, -1.0f, 1.0f,      1.0f, 1.0f, 1.0f,
             1.0f, 1.0f, 1.0f,       1.0f, 1.0f, -1.0f,      1.0f, -1.0f, -1.0f,

            -1.0f, -1.0f, 1.0f,     -1.0f, 1.0f, 1.0f,       1.0f, 1.0f, 1.0f,
             1.0f, 1.0f, 1.0f,       1.0f, -1.0f, 1.0f,     -1.0f, -1.0f, 1.0f,

            -1.0f, 1.0f, -1.0f,      1.0f, 1.0f, -1.0f,      1.0f, 1.0f, 1.0f,
             1.0f, 1.0f, 1.0f,      -1.0f, 1.0f, 1.0f,      -1.0f, 1.0f, -1.0f,
             
            -1.0f, -1.0f, -1.0f,    -1.0f, -1.0f, 1.0f,      1.0f, -1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,    -1.0f, -1.0f, 1.0f,      1.0f, -1.0f, 1.0f
        };

    m_SizeVertices = sizeof(vertices);
        
    std::vector<glimac::ShapeVertex> data;

    for(GLsizei i = 0; i < 12*3; ++i) {
        glimac::ShapeVertex vertex;
        
        //calcul des coordonnées de texture selon la face du cube
        if (i % 6 == 0) {
            vertex.texCoords.x = 0.f;
            vertex.texCoords.y = 1.f;
        } else if (i % 6 == 1) {
            vertex.texCoords.x = 0.f;
            vertex.texCoords.y = 0.f;
        } else if (i % 6 == 2) {
            vertex.texCoords.x = 1.f;
            vertex.texCoords.y = 0.f;
        } else if (i % 6 == 3) {
            vertex.texCoords.x = 1.f;
            vertex.texCoords.y = 0.f;
        } else if (i % 6 == 4) {
            vertex.texCoords.x = 1.f;
            vertex.texCoords.y = 1.f;
        } else if (i % 6 == 5) {
            vertex.texCoords.x = 0.f;
            vertex.texCoords.y = 1.f;
        }

        vertex.position = glm::vec3(size*vertices[i*3],size*vertices[i*3+1],size*vertices[i*3+2]);
        vertex.normal = vertex.position;
        
        data.push_back(vertex); 
    }

    m_nVertexCount = data.size();

    for(GLsizei i=0; i<m_nVertexCount; i++){
        m_Vertices.push_back(data[i]); 
    }
}

}
