#pragma once
#include <glimac/Program.hpp>
#include "glimac/FilePath.hpp"
#include "glimac/Image.hpp"
#include <glimac/stb_image.hpp>
#include "Cubemap.hpp"

using namespace glimac;

struct SkyBoxProgram
{
    Program m_Program;

    GLint uMVMatrix;
    GLint uMVPMatrix;

    SkyBoxProgram(const FilePath &applicationPath) : 
        m_Program(loadProgram(applicationPath.dirPath() + "shaders/skybox.vs.glsl",
                              applicationPath.dirPath() + "shaders/skybox.fs.glsl"))
    {
        uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
        uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
    }
};

GLuint CreateSkyboxVAO();
GLuint CreateSkyboxVBO(gWorld::Cubemap skybox);
GLuint CreateModelSkybox(std::string pathProject_current,gWorld::Cubemap* skybox);
void DrawSkybox(std::string applicationPath, GLuint skyboxVAO,GLuint skyboxVBO,GLuint cubemapTexture,glm::mat4 MVMatrix,glm::mat4 ProjMatrix);
void freeSkybox(GLuint* skyboxVAO, GLuint* skyboxVBO);
