#pragma once
#include <GL/glew.h>
#include "Shape.hpp"

namespace openGL {

    class VBO {
        private : 
            GLuint m_vbo;
        
        public :
            VBO(gWorld::Shape &shape);
            ~VBO();

            inline GLuint* getVBO() { return &m_vbo;};
    };
    
}