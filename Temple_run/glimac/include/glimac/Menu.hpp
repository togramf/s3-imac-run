#pragma once

#include <glimac/Program.hpp>

#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

using namespace glimac;


/**
 * @brief ecrit le texte
 * 
 * @param text 
 * @param font 
 * @param color 
 * @param location 
 */
void SDL_GL_RenderText(char *text,TTF_Font *font,SDL_Color color,SDL_Rect *location);

void glEnable2D();
void glDisable2D();

TTF_Font* initializeTTF(std::string fontpath,int size);

void DrawMenu(TTF_Font *font2,TTF_Font *font1,int screenwidth, int screenheight, int MenuType, int Score);
void CloseFont(TTF_Font *font);