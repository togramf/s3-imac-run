#pragma once

#include <vector>
#include "common.hpp"
#include "Shape.hpp"

namespace gWorld {

    /**
     * @brief Classe Cone
     * Représente un cone ouvert discrétisé dont la base est centrée en (0, 0, 0) (dans son repère local)
     * Son axe vertical est (0, 1, 0) et ses axes transversaux sont (1, 0, 0) et (0, 0, 1)
     */
    class Cone : public Shape {

        /**
         * @brief Alloue et construit les données (implantation dans le .cpp)
         * 
         * @param height 
         * @param radius 
         * @param discLat 
         * @param discHeight 
         */
        void build(GLfloat height, GLfloat radius, GLsizei discLat, GLsizei discHeight);

    public:
        // Constructeur: alloue le tableau de données et construit les attributs des vertex
        Cone(GLfloat height, GLfloat radius, GLsizei discLat, GLsizei discHeight):
            Shape() {
            build(height, radius, discLat, discHeight); 
        }
    };
    
}
