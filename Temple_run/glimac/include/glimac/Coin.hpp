#pragma once

#include "glm.hpp"
#include "Sphere.hpp"
#include "Camera.hpp"
#include <iostream>
#include <list>

namespace gWorld {

    /**
     * @brief Classe pièces
     * 
     */
    class Coin {

        private :
            glm::vec3   m_Position; 
            Shape*      m_body;
            int         m_value;
            bool        m_catched;
        
        public : 
            //constructeur et destructeur 
            Coin ();
            Coin (glm::vec3 Position);
            ~Coin() = default;

            //fonctions
            void                        isCatched();
        
            //getter
            const glm::vec3             getPosition();
            const int                   getValue() const;
            const glimac::ShapeVertex*  getBodyPointer() const;
            GLsizei                     getVertexCount() const;
            inline Shape*               getBody() {return m_body;};
            bool                        getCatched() const;

    };
}