#pragma once

#include "glm.hpp"
#include "Camera.hpp"

namespace gWorld {

    class TrackballCamera : public Camera {
        
    private:
        float m_fDistance;  //distance par rapport au centre de la scène
       
    public:
        //constructeur et destructeur
        TrackballCamera();
        TrackballCamera(const float d,const float x,const float y);
        ~TrackballCamera() = default;

        //méthodes de déplacement 
        void moveFront(const float t) override;
        void moveLeft(const float t) override;
        void rotateLeft(const float degrees) override;
        void rotateUp(const float degrees) override;
        void getPosition();
        void initialise() override;
        void initialise(const float d,const float x,const float y);

        /**
         * @brief Get the View Matrix object
         * 
         * @return glm::mat4 
         */
        glm::mat4 getViewMatrix() const;
    };
}