#pragma once

#include "glm.hpp"
#include "Camera.hpp"

namespace gWorld {

    class FreeflyCamera : public Camera {
        
    private:
        glm::vec3 m_Position;   //position de la cam par rapport à la target
        
        glm::vec3 m_FrontVector;    // vecteur F
        glm::vec3 m_LeftVector;     // vecteur L
        glm::vec3 m_UpVector;       // vecteur U

        void computeDirectionVectors();

    public:
        //constructeur et destructeur
        FreeflyCamera();
        FreeflyCamera(const glm::vec3 position);
        ~FreeflyCamera() = default;

        //méthodes de déplacement 
        void moveFront(const float t) override;
        void moveLeft(const float t) override;
        void rotateLeft(const float degrees) override;
        void rotateUp(const float degrees) override;
        void getPosition();
        void initialise() override;

        //getter
        glm::mat4 getViewMatrix() const;
    };
        
}