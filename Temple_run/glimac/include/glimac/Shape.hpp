#pragma once

#include <vector>

#include "common.hpp"

namespace gWorld {
    class Shape {

    protected:
        std::vector<glimac::ShapeVertex> m_Vertices; // Vecteur contenant les sommets
        GLsizei m_nVertexCount; // Nombre de sommets

    public:
        
        Shape() : m_nVertexCount(0){};

        // Renvoit le pointeur vers les données
        const glimac::ShapeVertex* getDataPointer() const {
            return &m_Vertices[0];
        }
        
        // Renvoit le nombre de vertex
        GLsizei getVertexCount() const {
            return m_nVertexCount;
        }
    };
    
    
}