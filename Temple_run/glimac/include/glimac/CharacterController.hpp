#pragma once

#include <GL/glew.h>
#include "Character.hpp"
#include "Coin.hpp"
#include "SDLWindowManager.hpp" // to handle mouse events
#include <iostream>

namespace gWorld {

    class CharacterController {
        private:
            Character* _character;
            float _speed = -0.1;

        public:
            //constructeur et destructeur
            CharacterController(Character* chara) : _character(chara) { };
            ~CharacterController();

            /**
             * @brief gère les évènements concernant le personnage
             * 
             * @param windowManager 
             * @param e 
             */
            void onEvent (const glimac::SDLWindowManager& windowManager, const SDL_Event& e);
            /**
             * @brief à chaque tour de loop, 
             * met à jour le personnage en fonction de son état
             */
            void update (std::vector<gWorld::Coin*> &coinsPosition, std::vector<glm::vec3> &wallsPositions, bool &endGame); 

        
    };
}