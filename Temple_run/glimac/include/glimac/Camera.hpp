#pragma once

#include "glm.hpp"

namespace gWorld {

    /**
     * @brief classe abstraite Camera
     * Observer d'une cible
     * 
     */
    class Camera {
        protected :
            glm::vec3   m_targetPosition; //position de la cible de la camera
            float       m_fAngleX;        //angle autour de l'axe X (bas ou haut)
            float       m_fAngleY;        //angle autour de l'axe Y (rotation droite ou gauche)
        
        public : 
            //Constructeur et destructeur
            Camera(const float x, const float y) : m_fAngleX(x), m_fAngleY(y) {};
            virtual ~Camera()=default;

            //fonctions de déplacement de la camera
            virtual void moveFront  (const float t) = 0;
            virtual void moveLeft   (const float t) = 0;
            virtual void rotateUp   (const float degrees) = 0;
            virtual void rotateLeft (const float degrees) = 0;

            /**
             * @brief réinitialise la position de la caméra
             * 
             */
            virtual void initialise() = 0;
            
            /**
             * @brief met à jour la position de la cible 
             * 
             * @param targetPosition 
             */
            void updateTargetPos(const glm::vec3 targetPosition) {
                m_targetPosition = targetPosition;
            };

            /**
             * @brief Donne la ViewMatrix 
             * 
             * @return glm::mat4 
             */
            virtual glm::mat4 getViewMatrix() const = 0;

    };
}
