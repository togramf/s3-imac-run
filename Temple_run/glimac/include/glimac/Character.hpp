#pragma once

#include "glm.hpp"
#include "Shape.hpp"
#include "Sphere.hpp"
#include "Camera.hpp"
#include <iostream>
#include <list>
#include <string>

namespace gWorld {

    class Character {

        private :
            glm::vec3    m_Position;       // position dans le monde
            glm::vec3    m_FrontVector;    // vecteur F
            glm::vec3    m_LeftVector;     // vecteur L
            glm::vec3    m_UpVector;       // vecteur U
            Shape*       m_body;             
            std::list<Camera*> m_Observers;
            float       m_fAngleX;       //angle autour de l'axe X (bas ou haut)
            float       m_fAngleY;       //angle autour de l'axe Y (rotation droite ou gauche)
            int         m_NbCoins = 0;
            bool        m_centered;
            bool        m_running;
            bool        m_jumping;
            bool        m_squatting;
            bool        m_left;
            bool        m_right;

        public : 
            //constructeur et destructeur
            Character ();
            ~Character() = default;
            
            //gestions des observers
            void add    (Camera& cam);
            void remove (Camera& cam);
            void notify ();
            
            //mouvements
            void start          ();
            void moveFront      (float t);
            void right          ();
            void left           ();
            void moveLeft       (float t);
            void jump           ();
            void squat          ();
            void moveUp         (float t);
            void centered       ();
            
            //score
            void updateNbCoin  (int valeur);

            //getter
            const glm::vec3             getPosition();
            const int                   getNbCoins();
            const glimac::ShapeVertex*  getBodyPointer() const;
            inline Shape*               getBody() {return m_body;};
            GLsizei                     getVertexCount() const;
            bool                        getState(const std::string input) const;

            

    };
}