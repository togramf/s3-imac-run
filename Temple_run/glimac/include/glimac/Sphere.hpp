#pragma once

#include <vector>

#include "common.hpp"
#include "Shape.hpp"

namespace gWorld {

    /**
     * @brief classe sphere
     * Représente une sphère discrétisée centrée en (0, 0, 0) (dans son repère local)
     * Son axe vertical est (0, 1, 0) et ses axes transversaux sont (1, 0, 0) et (0, 0, 1)
     */
    class Sphere : public Shape {

        /**
         * @brief Alloue et construit les données (implantation dans le .cpp)
         * 
         * @param radius 
         * @param discLat 
         * @param discLong 
         */
        void build(GLfloat radius, GLsizei discLat, GLsizei discLong);

    public:
        // Constructeur: alloue le tableau de données et construit les attributs des vertex
        Sphere(GLfloat radius, GLsizei discLat, GLsizei discLong):
            Shape() {
            build(radius, discLat, discLong); // Construction (voir le .cpp)
        }
    };
    
}