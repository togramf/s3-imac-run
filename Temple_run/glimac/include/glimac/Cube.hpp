#pragma once

#include <vector>

#include "common.hpp"
#include "Shape.hpp"

namespace gWorld {

    /**
     * @brief Classe Cube
     * Représente un cube centré en (0, 0, 0)
     */
    class Cube : public Shape {

        /**
         * @brief Alloue et construit les données (implantation dans le .cpp)
         * 
         * @param side 
         */
        void build(const GLfloat side);

    private:
        GLsizei m_SizeVertices;

    public:
        // Constructeur: alloue le tableau de données et construit les attributs des vertex
        Cube(const GLfloat side): Shape()
        {
            build(side); 
        }
    };
        
}