#pragma once

#include <GL/glew.h>
#include "Camera.hpp"
#include "TrackballCamera.hpp"
#include "FreeflyCamera.hpp"
#include "SDLWindowManager.hpp" //pour pouvoir récupérer les eventements de la souris 
#include <iostream>

namespace gWorld {

    class CameraController {
        private:
            int _camActive;     //entier indiquant la camera activée
            TrackballCamera* _camTEyes; // camera 1
            FreeflyCamera* _camF;       // camera 2
            TrackballCamera* _camT;     // camera 3
            bool _camIsBlocked;         
            float pasT = 5.0;
            float pas_move = 0.1;
            float pas_angle = 1.0;

            void initialiseCamPos();
        public:
            //constructeur et desctructeur 
            CameraController() : _camActive(3), _camIsBlocked(false) {
                _camT       = new TrackballCamera(-6.f, 15.0,0.0);        //camera centrée sur l'explorateur, à la troisième personne
                _camTEyes   = new TrackballCamera();                      //camera à la première personne 
                _camF       = new FreeflyCamera(glm::vec3(0.,1,6.f));     //camera libre 
            };
            ~CameraController();

            /**
             * @brief gère les evenements de la fenêtre concertant les cameras 
             * 
             * @param windowManager 
             * @param e 
             */
            void onEvent (const glimac::SDLWindowManager& windowManager, const SDL_Event& e);
            
            //getter 
            glm::mat4 getViewMatrix() const;
            inline Camera* getPointerCameraT(){ return _camT; };
            inline Camera* getPointerCameraTEyes(){ return _camTEyes; };
            inline Camera* getPointerCameraF(){ return _camF; };

    };
}