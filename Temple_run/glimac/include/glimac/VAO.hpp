#pragma once
#include <GL/glew.h>
#include "VBO.hpp"

namespace openGL {

    /**
     * @brief Classe VAO
     * 
     */
    class VAO {
        private : 
            GLuint m_vao;
        
        public :
            VAO(VBO &vbo, GLuint attr_pos, GLuint attr_normal,
                GLuint attr_coord);
            ~VAO();

            inline GLuint* getVAO() { return &m_vao;};
    };
    
}