#pragma once

#include <GL/glew.h>
#include <vector>
#include <iostream>

namespace gWorld {

    /**
     * @brief Classe cubemap pour la skybox
     * 
     */
    class Cubemap {
        
        public : 
            // constructeur et destructeur 
            Cubemap(const std::string pathProject, const std::string map);
            ~Cubemap()=default;

            // renvoit le pointeur vers les points
            const GLfloat* getVerticesPointer() const {
                return &m_vertices[0];
            };

            // renvoit le nombre de points
            GLsizei getVerticesLength() const {
                return m_size_vertices;
            };

            // renvoit le nombre de faces 
            GLsizei getFacesLength() const {
                return 6;
            };

            // renvoit les faces 
            const std::string getFace(const int i){
                return m_faces[i];
            };

        private : 
            std::vector<GLfloat>        m_vertices;
            GLsizei                     m_size_vertices;
            std::vector<std::string>    m_faces;
    };

}