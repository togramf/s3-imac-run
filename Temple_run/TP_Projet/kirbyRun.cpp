#include <GL/glew.h>
#include <iostream>
#include <vector>

#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include "glimac/glm.hpp"
#include <glimac/SDLWindowManager.hpp>
#include "glimac/Image.hpp"
#include <glimac/stb_image.hpp>
//VAO et VBO
#include "glimac/VBO.hpp"
#include "glimac/VAO.hpp"
//cameras 
#include "glimac/CameraController.hpp"
//classes d'objets affichables
#include "glimac/Cubemap.hpp"
#include "glimac/Sphere.hpp"
#include "glimac/Cube.hpp"
#include "glimac/Coin.hpp"
//personnage
#include "glimac/CharacterController.hpp"
#include "glimac/Skybox.hpp"
#include "glimac/Menu.hpp"

using namespace glimac;

struct SimpleTextureProgram {
    Program m_Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    GLint uTexture;

    SimpleTextureProgram(const FilePath& applicationPath):
        m_Program(loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                              applicationPath.dirPath() + "shaders/tex3D.fs.glsl")) {
        uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
        uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
        uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
        uTexture = glGetUniformLocation(m_Program.getGLId(), "uTexture");
    }
};

struct DoubleTextureProgram {
    Program m_Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    GLint uMainTexture;
    GLint uSecondTexture;

    DoubleTextureProgram(const FilePath& applicationPath):
        m_Program(loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                              applicationPath.dirPath() + "shaders/multiTex3D.fs.glsl")) {
        uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
        uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
        uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
        uMainTexture = glGetUniformLocation(m_Program.getGLId(), "uTexture");
        uSecondTexture = glGetUniformLocation(m_Program.getGLId(), "uTexture2");
    }
};

int main(int argc, char** argv) {

    // Setup environnement de travail 
    std::string pathProject_Vina = "/home/elvina/Documents/IMAC/S3/s3-imac-run/Temple_run";
    std::string pathProject_Gogo = "/home/togram/Documents/IMAC/SI/S3-Imac'Run/Temple_run";

    std::string pathProject_current = pathProject_Vina; // LIGNE A CHANGER EN FONCTION DE L'ENVT DE TRAVAIL

    // Initialize SDL and open a window
    int screenwidth=800; 
    int screenheight=600;
    SDLWindowManager windowManager(screenwidth, screenheight, "Kirby Run");
	
    gWorld::CameraController* camController = new gWorld::CameraController();

    //Initialize Font
    std::string fontpath1 = pathProject_current+"/assets/fonts/Gamer.ttf";
    TTF_Font *font1 = initializeTTF(fontpath1, 30);

    std::string fontpath2 = pathProject_current+"/assets/fonts/Game_On.ttf";
    TTF_Font *font2 = initializeTTF(fontpath2,80);

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    //chargement des textures
    std::unique_ptr<Image> kirby = loadImage (pathProject_current+"/assets/textures/items/kirby.png");
    if(!kirby) {
        std::cerr << "erreur chargement texture" << std::endl;
        return EXIT_FAILURE;
    }
    std::unique_ptr<Image> floor = loadImage (pathProject_current+"/assets/textures/items/flagstones.png");
    if(!floor) {
        std::cerr << "erreur chargement texture" << std::endl;
        return EXIT_FAILURE;
    }
    std::unique_ptr<Image> floor2 = loadImage (pathProject_current+"/assets/textures/items/stars.png");
    if(!floor2) {
        std::cerr << "erreur chargement texture" << std::endl;
        return EXIT_FAILURE;
    }
    std::unique_ptr<Image> coin_text = loadImage (pathProject_current+"/assets/textures/items/coin.png");
    if(!coin_text) {
        std::cerr << "erreur chargement texture" << std::endl;
        return EXIT_FAILURE;
    }
   
    //chargement des shaders
    FilePath applicationPath(argv[0]);
    DoubleTextureProgram doubleProgram(applicationPath);
    SimpleTextureProgram simpleProgram(applicationPath);

    //initialisation des matrices 
    glm::mat4 ProjMatrix = glm::perspective(glm::radians(70.f),(float)800/(float)600,0.1f,100.f);
    glm::mat4 MVMatrix = camController->getViewMatrix();
    glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    
    /*********************************
     *     INITIALIZATION CODE
     *********************************/
    
    //creation des objets unitaires
    gWorld::Character* personnage = new gWorld::Character();
    gWorld::CharacterController* charaController = new gWorld::CharacterController(personnage);
    gWorld::Cube cube(1.);
    gWorld::Coin* coin = new gWorld::Coin();

    //association du personnage et des caméras
    personnage->add(*camController->getPointerCameraF());
    personnage->add(*camController->getPointerCameraT());
    personnage->add(*camController->getPointerCameraTEyes());
    
    // Creation des vao et des vbo
    const GLuint VERTEX_ATTR_POSITION = 0;
    const GLuint VERTEX_ATTR_NORMALE = 1;
    const GLuint VERTEX_ATTR_COORDS = 2;

        //perso 
        openGL::VBO* vbo_perso = new openGL::VBO(*personnage->getBody());
        openGL::VAO* vao_perso = new openGL::VAO(*vbo_perso,VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMALE, VERTEX_ATTR_COORDS);

        //cube
        openGL::VBO* vbo_cube = new openGL::VBO(cube); 
        openGL::VAO* vao_cube = new openGL::VAO(*vbo_cube,VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMALE, VERTEX_ATTR_COORDS);
  
        //piece
        openGL::VBO* vbo_coin = new openGL::VBO(*coin->getBody()); 
        openGL::VAO* vao_coin = new openGL::VAO(*vbo_coin,VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMALE, VERTEX_ATTR_COORDS);
  
        // skybox
        std::string skybox_map = "sky";
        gWorld::Cubemap skybox = gWorld::Cubemap(pathProject_current, skybox_map);

        GLuint skyboxVAO = CreateSkyboxVAO();
        GLuint skyboxVBO = CreateSkyboxVBO(skybox);

        glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
        glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid *)0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        GLuint cubemapTexture = CreateModelSkybox(pathProject_current,&skybox);

    // Création de textures 
        // personnage
    GLuint texture_perso;
    glGenTextures(1, &texture_perso);
    glBindTexture(GL_TEXTURE_2D, texture_perso);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, kirby->getWidth(), kirby->getHeight(), 0, GL_RGBA, GL_FLOAT, kirby->getPixels());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);

        // pieces
    GLuint texture_coin; 
    glGenTextures(1, &texture_coin);
    glBindTexture(GL_TEXTURE_2D, texture_coin);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, coin_text->getWidth(), coin_text->getHeight(), 0, GL_RGBA, GL_FLOAT, coin_text->getPixels());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);

        //stars
    GLuint texture_stars;
    glGenTextures(1, &texture_stars);
    glBindTexture(GL_TEXTURE_2D, texture_stars);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, floor2->getWidth(), floor2->getHeight(), 0, GL_RGBA, GL_FLOAT, floor2->getPixels());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);

        // floor
    GLuint texture_floor;
    glGenTextures(1, &texture_floor);
    glBindTexture(GL_TEXTURE_2D, texture_floor);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, floor->getWidth(), floor->getHeight(), 0, GL_RGBA, GL_FLOAT, floor->getPixels());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);

    //Tirage des positions des pièces 
    uint32_t nbCoin = 28;
    std::vector<gWorld::Coin*> VecCoins;
    
    for (uint32_t i=0; i<nbCoin; ++i){
        gWorld::Coin* coin = new gWorld::Coin(glm::vec3(0.0, glm::linearRand(0.5,2.5),-3.0-3.5*i));
        VecCoins.push_back(coin);
    }

    //Tirage des positions des murs 
    float lastWallz = -3.0;
    std::vector<glm::vec3> VecWallsPositions;
    
    while (lastWallz > -100. + 10.){
        lastWallz -= glm::linearRand(6.,20.);
        glm::vec3 wall(glm::linearRand(-3.,3.), 0.0,lastWallz);
        VecWallsPositions.push_back(wall);
    }

    /*********************************
     *             LOOP
     *********************************/

    bool done = false;      bool end = false;
    bool showMenu = true;   bool InSubMenu = false;
    int MenuType = 0;       

    while(!done) {
        //récupération du score du personnage 
        int Score = personnage->getNbCoins(); 

        if(personnage->getPosition().z<-100)
            end=true;   

        SDL_Event e;
        while(windowManager.pollEvent(e)) {
            switch (e.type){
                case SDL_QUIT:
                    done = true; // Fermeture de la fenetre
                    break;
                
                case SDL_KEYDOWN:
                    if(!showMenu){
                        switch(e.key.keysym.sym) {
                            case SDLK_ESCAPE:
                                showMenu=!showMenu;
                                MenuType=0;
                                break;
                        }
                        break;
                    }else{
                        switch(e.key.keysym.sym) {
                            case SDLK_i:
                                MenuType=2;
                                InSubMenu = true;
                                break;
                            case SDLK_s:    
                                MenuType=1;
                                InSubMenu = true;
                                break;
                            case SDLK_ESCAPE:
                                if(!InSubMenu)
                                    showMenu=!showMenu;
                                MenuType = 0;
                                InSubMenu = false;
                                break;
                            
                        }
                        break;
                    }
                break;
            } 
            camController->onEvent(windowManager, e);
            charaController->onEvent(windowManager, e);
        }

        /*********************************
         *         RENDERING CODE
         *********************************/
        
        if(showMenu && !end) {
            DrawMenu(font2, font1, screenwidth, screenheight, MenuType, Score);     
        } else if(!end){ 
            
            // Nettoie le buffer à chaque loop
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            // Prevent that your skybox draws depth values, and the skybox will never be in front of anything that will be drawn later
            glDisable(GL_DEPTH_TEST);

            // Get la ViewMatrix
            MVMatrix = glm::mat4(glm::mat3(camController->getViewMatrix()));

            // Skybox
            DrawSkybox(applicationPath, skyboxVAO, skyboxVBO, cubemapTexture, MVMatrix, ProjMatrix);

            // Get the ViewMatrix
            MVMatrix = camController->getViewMatrix();

            // Mise à jour du personnage
            charaController->update(VecCoins, VecWallsPositions, end);

            // Dessin du personnage 
            glBindVertexArray(*vao_perso->getVAO());
            simpleProgram.m_Program.use();
            glUniform1i(simpleProgram.uTexture, 0);

            glm::mat4 persoMVMatrix = MVMatrix;
            persoMVMatrix = glm::translate(persoMVMatrix, personnage->getPosition());
            persoMVMatrix = glm::rotate(persoMVMatrix, 3*windowManager.getTime(), glm::vec3(-1, 0, 0));
            
            glUniformMatrix4fv(simpleProgram.uMVMatrix, 1, GL_FALSE, 
                glm::value_ptr(persoMVMatrix));
            glUniformMatrix4fv(simpleProgram.uNormalMatrix, 1, GL_FALSE, 
                glm::value_ptr(glm::transpose(glm::inverse(persoMVMatrix))));
            glUniformMatrix4fv(simpleProgram.uMVPMatrix, 1, GL_FALSE, 
                glm::value_ptr(ProjMatrix * persoMVMatrix));

            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture_perso);

            glDrawArrays(GL_TRIANGLES, 0, personnage->getVertexCount());

            // Pieces 
            glBindVertexArray(*vao_coin->getVAO());
            simpleProgram.m_Program.use();
            glUniform1i(simpleProgram.uTexture, 0);
            
            for (uint32_t i=0; i<nbCoin; i++){
                if (!VecCoins[i]->getCatched()){
                    glm::mat4 coinMVMatrix = MVMatrix;
                    coinMVMatrix = glm::translate(coinMVMatrix, VecCoins[i]->getPosition());
                    coinMVMatrix = glm::scale(coinMVMatrix,glm::vec3(0.5,0.5,0.5));
                    coinMVMatrix = glm::rotate(coinMVMatrix, windowManager.getTime(), glm::vec3(0, 1, 0));

                    glUniformMatrix4fv(simpleProgram.uMVMatrix, 1, GL_FALSE, 
                        glm::value_ptr(coinMVMatrix));
                    glUniformMatrix4fv(simpleProgram.uNormalMatrix, 1, GL_FALSE, 
                        glm::value_ptr(glm::transpose(glm::inverse(coinMVMatrix))));
                    glUniformMatrix4fv(simpleProgram.uMVPMatrix, 1, GL_FALSE, 
                        glm::value_ptr(ProjMatrix * coinMVMatrix));

                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, texture_coin);

                    glDrawArrays(GL_TRIANGLES, 0, coin->getVertexCount());
                }
            }
       
            // Sols 
            glBindVertexArray(*vao_cube->getVAO());
            doubleProgram.m_Program.use();
            glUniform1i(doubleProgram.uMainTexture, 0);
            glUniform1i(doubleProgram.uSecondTexture, 1);
        
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture_floor);
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, texture_stars);
            
            for (uint32_t i=0; i<50; i++){
                glm::mat4 cubeMVMatrix = glm::translate(MVMatrix, glm::vec3(0.,-1.2,0.));
                cubeMVMatrix = glm::scale(cubeMVMatrix, glm::vec3(5, 0.3, 5));
                cubeMVMatrix = glm::translate(cubeMVMatrix, glm::vec3(0.,0.,-2.*i));

                glUniformMatrix4fv(doubleProgram.uMVMatrix, 1, GL_FALSE,
                    glm::value_ptr(cubeMVMatrix));
                glUniformMatrix4fv(doubleProgram.uNormalMatrix, 1, GL_FALSE, 
                    glm::value_ptr(glm::transpose(glm::inverse(cubeMVMatrix))));
                glUniformMatrix4fv(doubleProgram.uMVPMatrix, 1, GL_FALSE, 
                    glm::value_ptr(ProjMatrix * cubeMVMatrix));

                glDrawArrays(GL_TRIANGLES, 0, cube.getVertexCount());
            }

            //Murs 
            for (uint32_t i =0; i < VecWallsPositions.size(); ++i){
                glm::mat4 wallMVMatrix = glm::translate(MVMatrix, VecWallsPositions[i]);
                wallMVMatrix = glm::scale(wallMVMatrix, glm::vec3(2.5, 0.8, 0.5));

                glUniformMatrix4fv(doubleProgram.uMVMatrix, 1, GL_FALSE,
                    glm::value_ptr(wallMVMatrix));
                glUniformMatrix4fv(doubleProgram.uNormalMatrix, 1, GL_FALSE, 
                    glm::value_ptr(glm::transpose(glm::inverse(wallMVMatrix))));
                glUniformMatrix4fv(doubleProgram.uMVPMatrix, 1, GL_FALSE, 
                    glm::value_ptr(ProjMatrix * wallMVMatrix));

                glDrawArrays(GL_TRIANGLES, 0, cube.getVertexCount());

            }
            
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, 0);

            glUseProgram(0);
            glBindVertexArray(0);
        
        } else { 
            //fin de la partie 
            DrawMenu(font2, font1, screenwidth, screenheight, 3, Score); 
        }
        // Mise à jour de l'affichage 
        windowManager.swapBuffers();
    }

    //Libération de la mémoire
    delete(vao_perso);     delete(vbo_perso);
    delete(vao_cube);      delete(vbo_cube);
    delete(vao_coin);      delete(vbo_coin);
    
    glDeleteTextures(1, &texture_perso); 
    glDeleteTextures(1, &texture_floor);
    glDeleteTextures(1, &texture_coin);
    glDeleteTextures(1, &texture_stars);

    freeSkybox(&skyboxVAO,&skyboxVBO);
    CloseFont(font1);
    CloseFont(font2);

    return EXIT_SUCCESS;
}
