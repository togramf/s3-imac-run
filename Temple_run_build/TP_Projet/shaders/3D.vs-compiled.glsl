#version 300 es
#define GLSLIFY 1

layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec3 aVertexNormal;
layout(location = 2) in vec3 aVertexCoords;

uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat4 uNormalMatrix;

out vec3 vVertexPosition;
out vec3 vVertexNormal;
out vec2 vVertexCoords;

void main() {
    vVertexPosition = (uMVMatrix * vec4(aVertexPosition,1)).xyz;
    vVertexNormal = (uNormalMatrix * vec4(aVertexNormal,1)).xyz;
    vVertexCoords = aVertexCoords.xy;
    gl_Position = vec4(uMVPMatrix * vec4(aVertexPosition,1));
};
