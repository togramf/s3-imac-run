# S3-KIRBYRUN
Projet d'IMAC2 - 2021-2022
Elvina Chevalier et Margot Fauchon

## Mode d'emploi
### Installations à faire 

Il faut installer la SDL_ttf et freetype avec les commandes suivante 
    sudo apt-get install libsdl-ttf2.o-dev
    sudo apt-get install libfreetype6-dev

### Environnement 
Pour pouvoir accéder aux assets, il faut renseigner au programme le chemin du jeu en absolu.
Pour ce faire dans kirbyRun.cpp, à la ligne 70, il faut remplacer pathCurrent_project par "/home/session/Documents/DossierDuProjet/Temple-Run" par exemple. 

### Compilation
Pour compiler le fichier, il faut créer un dossier build à la racine du projet, l’ouvrir dans un terminal et exécuter les commandes suivantes : 
    cmake ../Temple_run
    make 

### Lancement du jeu 
Pour lancer le jeu, il faut maintenant taper dans le build : 
    TP_Projet/TP_Projet_kirbyRun

## Jouer
Une fois le jeu lancer, le menu explique les différentes commandes possibles : 

### Menu
S - Pour accéder au score 
I - Pour voir les explications 
ECHAP - Pour lancer le jeu 

### Jeu
ESPACE - Pour lancer la partie
ECHAP - Pour rouvrir le menu 

Controle du personnage : 
Z - Pour sauter 
Q - Pour décaler le personnage vers la gauche
D - Pour décaler le personnage vers la droite 

Controle de la caméra : 
La souris - permet de changer d'angle de vue 
C - permet de changer de caméra
I - permet de réinitialiser la position de la caméra 
L - permet de bloquer la caméra à la position choisie 

